//Source code for catalytic combustion case //

//The objective of this code is to establish a use case with the
//Cantera C++ interface.

#include "cantera/base/Solution.h"
#include "cantera/thermo.h"
#include "cantera/oneD/Boundary1D.h"
#include "cantera/oneD/Domain1D.h"
#include "cantera/oneD/StFlow.h"

#include "oneDIncludes/Sim1D.h"

#include <iostream>
#include <vector>
#include <iomanip>

using namespace Cantera;

void run_program()
{
	// Mechanism file
	auto mechanism_file = "ptcombust.yaml";
	// Gas phase parameters
	auto initial_pressure = OneAtm;
	auto inlet_temperature = 300.0;
	auto mdot = 0.06; // kg/m^2/s
	auto composition1 = "H2:0.05, O2:0.21, N2:0.78, AR:0.01";
	auto composition2 = "CH4:0.095, O2:0.21, N2:0.78, AR:0.01";
	auto transport_model = "Mix";
	auto gasphase_name = "gas";

	// Create solution object for the gas phase
	auto solution = newSolution(mechanism_file, gasphase_name,
		                    transport_model);
	// pointers to access the thermodynamic state, and
	// transport objects of the solution wrapper
	auto gas_thermo = solution->thermo();
	auto gas_transport= solution->transport();
	// set the initial thermodynamic state of the gas phase
	// the composition is taken to be a Hydrogen/Air mixture
	// to begin with
	gas_thermo->setState_TPX(inlet_temperature, initial_pressure,
		      composition1);

	// Impinging jet implementation
	// Create Inlet Object
	auto inlet = new Inlet1D();
	// Create a steady flow instance with arguments as the
	// thermoPhase instance, nSpecies and 6 grid points
	auto flame = new StFlow(gas_thermo, gas_thermo->nSpecies(), 6);
	// Since we are running the impinging jet problem, we call the setAxisymmetricFlow method
	flame->setAxisymmetricFlow();

	// create a grid vector
	std::vector<double> grid{0.00, 0.02, 0.04, 0.06, 0.08, 0.10};

	// Create Non-reacting Surface Object 
	auto surface = new Surf1D();
	// Initially Surface Temperature is maintained at Gas Temperature.
	surface->setTemperature(inlet_temperature);

	// Begin FlameBase Constructor call
	flame->setupGrid(6, grid.data());
	// 	Begin Sim1D constructor call
	// 		Sim1D first creates a container for holding all the domains
	std::vector<Domain1D*> domains;
	domains.push_back(inlet);
	domains.push_back(flame);
	domains.push_back(surface);
	auto isInitialized = 0;
	// 		Sim1D then creates a Sim1D object for all the domains
	auto sim = new mySim1D(domains);
        // 	End Sim1D constructor call
	flame->setPressure(initial_pressure);
	// End FlameBase Constructor call

	// set inlet temp (same as initial gas temperature)
	inlet->setTemperature(inlet_temperature);
	// Set inlet mole fractions
	double X[gas_thermo->nSpecies()];
        gas_thermo->getMoleFractions(X);
	inlet->setMoleFractions(X);
	// set mass flow rate at inlet
	inlet->setMdot(mdot);

	// Initialize the solution
	if(isInitialized == 0){
		sim->resize();
		sim->getInitialSoln();
		std::vector<double> pos{0.0, 1.0};
		double vinlet = mdot/gas_thermo->density();
		std::vector<double> values{vinlet, 0.0};
		// 1 = 1st domain i.e flame object
		// 0 = component i.e. velocity
		sim->setProfile(1, 0, pos, values);
		//sim->setFlatProfile(1, 0, 0.4);
		isInitialized = 1;
	}
	sim->showSolution();

	sim->solve(0, false);

	std::cout << "Completed run_program method\n";

}

// Main program
int main()
{

    try
    {
	    run_program();
    }
    catch (CanteraError& err)
    {
        std::cout << err.what() << std::endl;
    }

    //std::cout << nStep << std::endl;
    //std::cout << dt << std::endl;
}
