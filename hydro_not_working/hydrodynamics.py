import numpy, cantera

p = cantera.one_atm  # pressure
tinlet = 300.0  # inlet temperature
mdot = 0.06  # kg/m^2/s
transport = 'Mix'  # transport model

comp1 = 'H2:0.05, O2:0.21, N2:0.78, AR:0.01'
width = 0.1  # m

gas = cantera.Solution("ptcombust.yaml", "gas", transport)

gas.TPX = tinlet, p, comp1

sim = cantera.ImpingingJet(gas=gas, width=width)

sim.inlet.mdot = mdot
sim.inlet.T = tinlet
sim.inlet.X = comp1

#sim.show_solution()

#sim.solve(loglevel=0, refine_grid=False, auto=False)

#sim.show_solution()
