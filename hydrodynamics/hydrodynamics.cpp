//Source code for catalytic combustion case //

//The objective of this code is to establish a use case with the
//Cantera C++ interface.

#include "cantera/base/Solution.h"
#include "cantera/thermo.h"
#include "cantera/oneD/Boundary1D.h"
#include "cantera/oneD/Domain1D.h"
#include "cantera/oneD/StFlow.h"
#include "cantera/oneD/Sim1D.h"

#include <iostream>
#include <vector>
#include <iomanip>

using namespace Cantera;

int hydrodynamic_impingingjet(std::string reaction_mechanism,
	      	              std::string transport_model,
	      	              std::string mole_fractions,
			      double inlet_temperature,
			      double surface_temperature,
			      std::string gasphase_name,
	      	              double pressure,
	      	              double mdot, 
			      bool refinegrid,
			      int loglevel,
			      bool isIsothermal)
{
    // modify surface temperature for isothermal case
    if(isIsothermal){
        surface_temperature = inlet_temperature;
        refinegrid = false;}
    // create solution object
    auto sol = newSolution(reaction_mechanism, gasphase_name, transport_model);
    // isolate pointers to the thermodynamics, kinetics and transport objects
    auto gas = sol->thermo();
    auto transport = sol->transport();
    auto kinetics = sol->kinetics();
    // isolate the number of species in the homogeneous phase
    int n_species = gas->nSpecies();
    // isolate the number of homogeneous reactions
    int n_reactions = kinetics->nReactions();

    // set initial state of the gas
    gas->setState_TPX(inlet_temperature, pressure, mole_fractions);

    // turn off chemistry
    for (int reaction = 0; reaction < n_reactions; ++reaction){
	    kinetics->setMultiplier(reaction, 0.0);
    }

    // create an initial grid
    int nz = 6;
    double lz = 0.1;
    double z[nz];
    double dz = lz/((double)(nz-1));
    for (int iz = 0; iz < nz; iz++) {
        z[iz] = ((double)iz)*dz;
    }

    std::vector<double> grid(z, z+nz);

    // Create the flow
    auto flow = new StFlow(gas, n_species, nz);
    // set flow to be axisymmetric for impinging jet case
    flow->setAxisymmetricFlow();
    // set the grid
    flow->setupGrid(nz, grid.data());
    // specify the objects to use to compute kinetic rates and
    // transport properties
    flow->setTransport(*transport);
    flow->setKinetics(*kinetics);
    // set the pressure
    flow->setPressure(pressure);
    // solve energy equation in the flow
    if (!isIsothermal){flow->solveEnergyEqn();}

    // Create the inlet
    auto inlet = new Inlet1D();

    // Create the non-reacting Surface
    auto surface = new Surf1D();
    surface->setTemperature(surface_temperature);
    
    // Create container and insert domains
    std::vector<Domain1D*> domains {inlet, flow, surface};
    //mySim1D sim(domains);
    Sim1D jet(domains);

    // !!! MUST INITIALIZE INLET AFTER CREATING THE CONTAINER !!! //
    
    inlet->setTemperature(inlet_temperature);
    double x[n_species];
    gas->getMoleFractions(x);
    inlet->setMoleFractions(x);
    inlet->setMdot(mdot);


    // Initialize solution with linear velocity and temperature
    // profile from inlet to surface
    jet.resize();
    jet.getInitialSoln();

    double v_inlet = mdot/gas->density();
    std::vector<double> pos {0.0, 1.0};
    std::vector<double> vel_values {v_inlet, 0.0};
    std::vector<double> temp_values {inlet_temperature, surface_temperature};

    jet.setProfile(1, 0, pos, vel_values);
    jet.setProfile(1, 2, pos, temp_values);

    jet.showSolution(); // show initial solution

    // solve
    jet.solve(loglevel, refinegrid);
    jet.showSolution();
    
    return 0;
}

// Main program
int main()
{
    double temperature = 300.0; // K
    double surface_temperature = 900.0; // K
    double pressure = 1.0*OneAtm; // atm
    double mdot = 0.06; // kg/m^2/s

    bool isIsothermal {}; // solve energy equation as default
    isIsothermal = true;

    std::string mechanism = "ptcombust.yaml";
    std::string transport_model = "Mix";
    std::string mix_mole_fractions = "H2:0.05, O2:0.21, N2:0.78, AR:0.01";
    std::string gasphase_name = "gas";

    int loglevel = 1;
    bool refinegrid {}; // do not refine grid by default
    if (!isIsothermal){refinegrid=true;} //refine grid for non-isothermal case
    
    try{
	hydrodynamic_impingingjet(mechanism, transport_model, mix_mole_fractions,
			          temperature, surface_temperature, gasphase_name,
				  pressure, mdot, refinegrid, loglevel, isIsothermal);}
    catch (CanteraError& err){
        std::cout << err.what() << std::endl;}
}
