import numpy, cantera

p = cantera.one_atm  # pressure
tinlet = 300.0  # inlet temperature
tsurface= 900.0 # surface temperature
mdot = 0.06  # kg/m^2/s
transport = 'Mix'  # transport model

# run isothermal case
isIsothermal = True

# modify surface temperature for isothermal case
if isIsothermal: tsurface = tinlet

# refine grid only for non-isothermal case
refine_grid=False if isIsothermal else True

comp1 = 'H2:0.05, O2:0.21, N2:0.78, AR:0.01'
width = 0.1  # m

gas = cantera.Solution("ptcombust.yaml", "gas", transport)

gas.TPX = tinlet, p, comp1

gas.set_multiplier(0.0) # turn off chemistry

sim = cantera.ImpingingJet(gas=gas, width=width)

sim.inlet.mdot = mdot
sim.inlet.T = tinlet
sim.inlet.X = comp1

sim.surface.T = tsurface

sim.show_solution()

sim.solve(loglevel=1, refine_grid=refine_grid, auto=False)

sim.show_solution()
